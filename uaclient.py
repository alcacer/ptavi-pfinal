#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys
import simplertp
import secrets
import hashlib
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import time


class SmallSMILHandler(ContentHandler):
    """Clase para manejar smil"""

    def __init__(self):
        self.lista_etiquetas = {}
        self.dic = {'account': ['username', 'passwd'],
                    'uaserver': ['ip', 'puerto'],
                    'rtpaudio': ['puerto'],
                    'regproxy': ['ip', 'puerto'],
                    'log': ['path'],
                    'audio': ['path']}

    def startElement(self, valor, atrib):
        if valor in self.dic:
            for item in self.dic[valor]:
                self.lista_etiquetas[valor + '-' + item] = atrib.get(item, '')

    def get_tags(self):
        return self.lista_etiquetas


def get_message(metodo):
    if metodo == 'INVITE':
        sdp = "v=0\r\no=" + configuracion["account-username"] \
              + ' ' + direccip + \
              "\r\ns=papisesion\r\nt=0\r\nm=audio " + rtpaudio + " RTP"
        line = 'INVITE sip:' + sys.argv[3] \
               + ' SIP/2.0\r\nContent-Type: ' \
                 'application/sdp\r\nContent-Lenght: ' \
               + str(len(sdp))
        line += '\r\n\r\n' + sdp + '\r\n'
    elif metodo == 'REGISTER':
        line = 'REGISTER sip:' \
               + configuracion['account-username'] + ':' + \
               configuracion['uaserver-puerto'] \
               + ' SIP/2.0\r\nExpires: ' \
               + sys.argv[3] + '\r\n'
    elif metodo == 'BYE':
        line = 'BYE sip:' + sys.argv[3] + ' SIP/2.0\r\n'
    else:
        line = metodo + ' sip:' + sys.argv[3] + ' SIP/2.0\r\n'
    return line


def send_rtp(my_socket, data, ip, port):
    LINE = 'ACK sip:' + sys.argv[3] + ' SIP/2.0\r\n\r\n'
    my_socket.send(bytes(LINE, 'utf-8'))
    log('Sent to ' + ip + ':' + str(port) + ': ' + LINE, LOGS)
    ip = data.split('\r\n')[9].split(' ')[1]
    port = int(data.split('\r\n')[12].split(' ')[1])
    BIT = secrets.randbelow(1)
    RTP_header = simplertp.RtpHeader()
    RTP_header.set_header(version=2, marker=BIT, payload_type=14, ssrc=200002)
    audio = simplertp.RtpPayloadMp3(configuracion['audio-path'])
    simplertp.send_rtp_packet(RTP_header, audio, ip, port)


def send_response(my_socket, data, line, ip, port):
    nonce = data.split("\"")[1]
    password = configuracion['account-passwd']
    resp = response(nonce, password)
    line += 'Authorization: Digest response="' + resp + '"\r\n\r\n'
    my_socket.send(bytes(line, 'utf-8'))
    log('Sent to ' + ip + ':' + str(port) + ': ' + line, LOGS)
    return my_socket.recv(1024).decode('utf-8')


def log(mensaje, log_path):
    """Abre un fichero log para poder escribir en el."""
    fich = open(log_path, "a")
    fich.write(time.strftime('%Y-%m-%d :%S'))
    fich.write(mensaje + "\r\n")
    fich.close()


def response(nonce, contrasena):
    digest = hashlib.sha224()
    digest.update(bytes(nonce + contrasena, 'utf-8'))
    digest.digest()
    return digest.hexdigest()


if __name__ == "__main__":
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    archivo = sys.argv[1]
    try:
        parser.parse(open(archivo))
    except FileNotFoundError:
        sys.exit('File not found')
    configuracion = cHandler.get_tags()
    print(configuracion)
    if configuracion['uaserver-ip'] == ' ':
        direccip = '127.0.0.1'
    else:
        direccip = configuracion['uaserver-ip']
    if configuracion['regproxy-ip'] == ' ':
        proxyip = '127.0.0.1'
    else:
        proxyip = configuracion['regproxy-ip']
    puerto = configuracion['uaserver-puerto']
    usuario = configuracion['account-username']
    contrasena = configuracion['account-passwd']
    rtpaudio = configuracion['rtpaudio-puerto']
    proxyport = int(configuracion['regproxy-puerto'])
    LOGS = configuracion['log-path']
    AUDIO = configuracion['audio-path']
    try:
        metodo = sys.argv[2]
        mensaje = sys.argv[3]
    except (IndexError, ValueError):
        sys.exit("Usage: client.py method receiver@IP:SIPport")
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((proxyip, proxyport))
        log(' Starting...', LOGS)
        LINE = get_message(metodo)
        my_socket.send(bytes(LINE + '\r\n', 'utf-8'))
        log('Sent to ' + proxyip + ':' + str(proxyport) + ': ' + LINE, LOGS)
        data = my_socket.recv(1024).decode('utf-8')
        log('Received from '
            + proxyip + ':'
            + str(proxyport) + ': '
            + data, LOGS)
        print(data)
        if 'SIP/2.0 401 Unauthorized' in data:
            data = send_response(my_socket, data, LINE, proxyip, proxyport)
            log('Received from '
                + proxyip + ':'
                + str(proxyport) + ': '
                + data, LOGS)
            print(data)
        if 'SIP/2.0 100 Trying' \
                in data and 'SIP/2.0 180 Ringing' \
                in data and 'SIP/2.0 200 OK' \
                in data:
            send_rtp(my_socket, data, proxyip, proxyport)
    log('Finishing', LOGS)
    print("Terminando socket...")
    print("Fin.")
