#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import simplertp
import secrets
from uaclient import SmallSMILHandler
from xml.sax import make_parser
import time
from uaclient import log


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    audiortp = []

    def receive_invite(self, texto):
        ip = texto.split('\r\n')[5].split(' ')[1]
        port = int(texto.split('\r\n')[8].split(' ')[1])
        LINE = 'SIP/2.0 100 Trying\r\n\r\nSIP/2.0 ' \
               '180 Ringing\r\n\r\nSIP/2.0 ' \
               '200 OK\r\n'
        sdp = "v=0\r\no=" \
              + configuracion["account-username"] + ' ' \
              + direccip + \
              "\r\ns=papisesion\r\nt=0\r\nm=audio " + str(rtpaudio) + " RTP"
        LINE += 'Content-Type: application/sdp\r\nContent-Lenght: ' \
                + str(len(sdp))
        LINE += '\r\n\r\n' + sdp + '\r\n\r\n'
        self.wfile.write(bytes(LINE, 'utf-8'))
        log('Sent to '
            + self.client_address[0] + ':'
            + str(self.client_address[1]) + ': '
            + LINE, LOGS)
        self.audiortp.append(ip)
        self.audiortp.append(port)

    def receive_ack(self):
        BIT = secrets.randbelow(1)
        RTP_header = simplertp.RtpHeader()
        RTP_header.set_header(version=2, marker=BIT,
                              payload_type=14,
                              ssrc=200002)
        audio = simplertp.RtpPayloadMp3(configuracion['audio-path'])
        simplertp.send_rtp_packet(RTP_header,
                                  audio,
                                  self.audiortp[0],
                                  self.audiortp[1])

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)

        texto = self.rfile.read().decode('utf-8')
        print(texto)
        metodo = texto.split(" ")[0]
        IP = self.client_address[0]
        PORT = self.client_address[1]
        log('Received from ' + IP + ':' + str(PORT) + ': ' + texto, LOGS)
        if metodo == 'INVITE':
            self.receive_invite(texto)
        elif metodo == 'ACK':
            self.receive_ack()
        elif metodo == 'BYE':
            self.audiortp = []
            self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
        else:
            self.wfile.write(b'SIP/2.0 405 Method Not Alowed\r\n\r\n')


if __name__ == "__main__":
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    archivo = sys.argv[1]
    try:
        parser.parse(open(archivo))
    except FileNotFoundError:
        sys.exit('FIle not found')
    configuracion = cHandler.get_tags()
    if configuracion['uaserver-ip'] == ' ':
        direccip = '127.0.0.1'
    else:
        direccip = configuracion['uaserver-ip']
    if configuracion['regproxy-ip'] == ' ':
        proxyip = '127.0.0.1'
    else:
        proxyip = configuracion['regproxy-ip']
    puerto = int(configuracion['uaserver-puerto'])
    usuario = configuracion['account-username']
    rtpaudio = int(configuracion['rtpaudio-puerto'])
    proxyport = int(configuracion['regproxy-puerto'])
    LOGS = configuracion['log-path']
    serv = socketserver.UDPServer((direccip, puerto), EchoHandler)
    print("Listening...")
    try:
        log('Starting...', LOGS)
        serv.serve_forever()
    except KeyboardInterrupt:
        log('Finishing.', LOGS)
        print("Finalizado servidor")
