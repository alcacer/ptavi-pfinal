#!/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import socket
import sys
import json
import time
import secrets
from uaclient import log
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import hashlib


class XMLHandler(ContentHandler):

    def __init__(self):
        self.list_etiq = {}
        self.etiqueta = ''
        self.dic = {'server': ['name', 'ip', 'puerto'],
                    'database': ['path', 'pswdpath'],
                    'log': ['']}

    def startElement(self, name, attrs):
        if name in self.dic:
            self.etiqueta = name
            for atributo in self.dic[name]:
                if name != 'log':
                    self.list_etiq[name
                                   + '/' + atributo] = attrs.get(atributo, '')

    def endElement(self, name):
        self.etiqueta = ''

    def characters(self, content):
        if self.etiqueta == 'log':
            self.list_etiq[self.etiqueta] = content

    def get_tags(self):
        return self.list_etiq


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dicc = {}
    diccpasswd = {}
    hexadecimal = {}

    def register2json(self):
        with open(client_db, 'w') as fichero_json:
            json.dump(self.dicc, fichero_json, indent=4)

    def json2passwords(self):
        try:
            with open(password_db, "r") as jsonfile:
                self.diccpasswd = json.load(jsonfile)
        except FileNotFoundError:
            pass

    def json2register(self):
        try:
            with open(client_db, "r") as jsonfile:
                self.dicc = json.load(jsonfile)
        except FileNotFoundError:
            pass

    def tiempoexpirado(self):
        tiempoactual = time.time() + 3600
        for email in self.dicc.copy():
            tiempocaduc = self.dicc[email][2] + self.dicc[email][3]
            if tiempoactual >= tiempocaduc:
                del self.dicc[email]

    def envioserver(self, ip, port, mensaje):

        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            my_socket.connect((ip, port))
            my_socket.send(bytes(mensaje, 'utf-8'))
            log('Sent to ' + ip + ':' + str(port) + ': ' + mensaje, LOGS)
            data = my_socket.recv(1024)
            log('Received from '
                + ip + ':' + str(port) + ': '
                + data.decode('utf-8'), LOGS)
        return data.decode('utf-8')

    def response(self, nonce, contrasena):
        digest = hashlib.sha224()
        digest.update(bytes(nonce + contrasena, 'utf-8'))
        digest.digest()
        return digest.hexdigest()

    def receive_invite(self, texto, ip, port):
        print(texto)
        destino = texto.split(' ')[1].split(':')[1]
        origen = texto.split('\r\n')[5].split(' ')[0].split('=')[1]
        if destino in self.dicc and origen in self.dicc:
            linea = self.envioserver(self.dicc[destino][0],
                                     self.dicc[destino][1],
                                     texto)
        else:
            linea = 'SIP/2.0 404 User Not Found\r\n\r\n'
        self.wfile.write(bytes(linea, 'utf-8'))
        linea2 = linea.replace("\r\n", " ")
        log('Sent to ' + ip + ':' + str(port) + ': ' + linea2, LOGS)

    def receive_register(self, texto, ip, port):
        print(texto)
        email = texto.split(" ")[1].split(':')[1]
        puerto = texto.split(" ")[1].split(':')[2]
        caducidad = int(texto.split("\r\n")[1].split(":")[1])
        tiempoactual = time.time() + 3600
        if email in self.dicc:
            if caducidad == 0:
                del self.dicc[email]
            elif caducidad != 0:
                self.dicc[email][2] = tiempoactual
                self.dicc[email][3] = caducidad
            linea = 'SIP/2.0 200 OK\r\n\r\n'
            self.wfile.write(bytes(linea, 'utf-8'))
            print('mandamos al cliente: ', linea)
            linea2 = linea.replace("\r\n", " ")
            log('Sent to ' + ip + ':' + str(port) + ': ' + linea2, LOGS)
        elif email not in self.dicc:
            if 'Authorization: Digest response' in texto:
                responseclient = texto.split('\r\n')[2].split('"')[1]
                response = self.response(self.hexadecimal[email],
                                         self.diccpasswd[email])
                if secrets.compare_digest(responseclient, response):
                    self.dicc[email] = [self.client_address[0],
                                        int(puerto),
                                        tiempoactual,
                                        caducidad]
                    linea = 'SIP/2.0 200 OK\r\n\r\n'
                    self.wfile.write(bytes(linea, 'utf-8'))
                    print('mandamos al cliente: ', linea)
                    linea2 = linea.replace("\r\n", " ")
                    log('Sent to '
                        + ip + ':'
                        + str(port)
                        + ': '
                        + linea2,
                        LOGS)
            else:
                caducidad = int(texto.split("\r\n")[1].split(":")[1])
                if caducidad != 0:
                    self.hexadecimal[email] = secrets.token_hex(2)
                    resp = 'SIP/2.0 401 Unauthorized\r\n' \
                           + 'WWW Authenticate: Digest nonce="' \
                           + self.hexadecimal[email] + '"\r\n\r\n'
                    self.wfile.write(bytes(resp, 'utf-8'))
                else:
                    linea = 'SIP/2.0 404 User Not Found\r\n\r\n'
                    self.wfile.write(bytes(linea, 'utf-8'))
                    print('mandamos al cliente: ', linea)
                    linea2 = linea.replace("\r\n", " ")
                    log('Sent to ' + ip + ':' + str(port)
                        + ': ' + linea2, LOGS)

    def receive_ack(self, texto, ip, port):
        destino = texto.split(' ')[1].split(':')[1]
        if destino in self.dicc:
            data = self.envioserver(self.dicc[destino][0],
                                    self.dicc[destino][1],
                                    texto)

        else:
            linea = 'SIP/2.0 404 User Not Found\r\n\r\n'
            self.wfile.write(bytes(linea, 'utf-8'))
            print('mandamos al cliente: ', linea)
            linea2 = linea.replace("\r\n", " ")
            log('Sent to ' + ip + ':' + str(port) + ': ' + linea2, LOGS)

    def receive_bye(self, texto, ip, port):
        destino = texto.split(' ')[1].split(':')[1]
        if destino in self.dicc:
            linea = self.envioserver(self.dicc[destino][0],
                                     self.dicc[destino][1],
                                     texto)
        else:
            linea = 'SIP/2.0 404 User Not Found\r\n\r\n'
        self.wfile.write(bytes(linea, 'utf-8'))
        print('mandamos al cliente: ', linea)
        linea2 = linea.replace("\r\n", " ")
        log('Sent to ' + ip + ':' + str(port) + ': ' + linea2, LOGS)

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        linea = self.rfile.read()
        texto = linea.decode('utf-8')
        self.json2passwords()
        self.json2register()
        self.tiempoexpirado()
        metodo = texto.split(" ")[0]
        IP = self.client_address[0]
        PORT = self.client_address[1]
        log('Received from ' + IP + ':' + str(PORT) + ': ' + texto, LOGS)
        if metodo in ["INVITE", "ACK", "BYE", "REGISTER"]:
            if texto.split(" ")[1].split(":")[0] != "sip":
                linea = 'SIP/2.0 400 Bad Request\r\n\r\n'
                self.wfile.write(bytes(linea, 'utf-8'))
                linea2 = linea.replace("\r\n", " ")
                log('Sent to ' + IP + ':' + str(PORT) + ': ' + linea2, LOGS)
                print('bad request from', IP, ':', PORT)
            else:
                print(metodo, 'received from', IP, ':', PORT)
                if metodo == 'INVITE':
                    self.receive_invite(texto, IP, PORT)
                elif metodo == 'ACK':
                    self.receive_ack(texto, IP, PORT)
                elif metodo == "BYE":
                    self.receive_bye(texto, IP, PORT)
                elif metodo == 'REGISTER':
                    self.receive_register(texto, IP, PORT)
        else:
            print(metodo, 'not allowed')
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n")
        self.register2json()


if __name__ == "__main__":
    parser = make_parser()
    cHandler = XMLHandler()
    parser.setContentHandler(cHandler)
    archivo = sys.argv[1]
    parser.parse(open(archivo))
    configuracion = cHandler.get_tags()
    PORT_server = int(configuracion['server/puerto'])
    IP_server = configuracion['server/ip']
    client_db = configuracion['database/path']
    password_db = configuracion['database/pswdpath']
    LOGS = configuracion['log']
    serv = socketserver.UDPServer((IP_server, PORT_server), SIPRegisterHandler)
    msg = 'server ' + configuracion['server/name'] \
          + ' listening at ' \
          + IP_server \
          + ' Port ' \
          + str(PORT_server)
    print(msg)
    try:
        log('Starting...', LOGS)
        serv.serve_forever()
    except KeyboardInterrupt:
        log('Finishing.', LOGS)
        print("Finalizado servidor")
